using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace FRRJIf
{
    using FanucInterface;

    public class DataTable
    {
        private List<TableItem> dataTable = new List<TableItem>();

        private static ushort Size = 0;

        internal DataTable()
        {
        }

        public bool Clear()
        {
            dataTable.Clear();
            return true;
        }

        public bool Refresh()
        {
            return true;
        }

        public int Count()
        {
            return dataTable.Count;
        }

        public object Item(object Index)
        {
            return dataTable[(int)Index];
        }

        public DataNumReg AddNumReg(FRIF_DATA_TYPE DataType, int StartIndex, int EndIndex)
        {
            DataNumReg dataNumReg = new DataNumReg();
            dataNumReg.DataTable = this;
            dataNumReg.StartIndex = StartIndex;
            dataNumReg.EndIndex = EndIndex;
            dataNumReg.DataType = DataType;
            dataNumReg.Offset = Size;
            ushort dataWordsLength = (ushort)(2 * (EndIndex - StartIndex));
            dataTable.Add(new TableItem(dataNumReg, new ListenerRequest($"SETASG {Size} {dataWordsLength} R[{StartIndex}] 1.0")));
            Size += dataWordsLength;
            return dataNumReg;
        }

        public DataPosReg AddPosReg(FRIF_DATA_TYPE DataType, int Group, int StartIndex, int EndIndex)
        {
            return new DataPosReg();
        }

        public DataCurPos AddCurPos(FRIF_DATA_TYPE DataType, int Group)
        {
            return new DataCurPos();
        }

        public DataTask AddTask(FRIF_DATA_TYPE DataType, int Index)
        {
            return new DataTask();
        }

        public DataSysVarPos AddSysVarPos(FRIF_DATA_TYPE DataType, string SysVarName)
        {
            return new DataSysVarPos();
        }

        public DataSysVar AddSysVar(FRIF_DATA_TYPE DataType, string SysVarName)
        {
            return new DataSysVar();
        }

        public DataAlarm AddAlarm(FRIF_DATA_TYPE DataType, int AlarmCount, int AlarmMessageMode = 0)
        {
            return new DataAlarm();
        }

        public bool GetLastDataFloat(int lngAddress, ref float sngValue)
        {
            return true;
        }

        public bool Fixed { get; }

        public int ObjectID { get; }

        public bool Valid { get; }

        public void Kill()
        {
        }

        public string GetTypeName()
        {
            return "";
        }

        public void AddUserMessage(string vstrMessage)
        {
        }

        public bool DebugLog { get { return Core.DebugLog; } }

        public DataPosRegXyzwpr AddPosRegXyzwpr(FRIF_DATA_TYPE DataType, int Group, int StartIndex, int EndIndex)
        {
            return new DataPosRegXyzwpr();
        }

        public DataCurPos AddCurPosUF(FRIF_DATA_TYPE DataType, int Group, int UF)
        {
            return new DataCurPos();
        }

        public DataString AddString(FRIF_DATA_TYPE DataType, int StartIndex, int EndIndex)
        {
            return new DataString();
        }

        public DataString AddStringEx(FRIF_DATA_TYPE DataType, string DataSymbol, int StartIndex, int EndIndex)
        {
            return new DataString();
        }
    }

    internal class TableItem
    {
        public object DataClass { get; private set; }
        public byte[] Message { get; private set; }

        public TableItem(object dataClass, IMessage message)
        {
            DataClass = dataClass;
            Message = message.MessageFrame;
        }
    }
}