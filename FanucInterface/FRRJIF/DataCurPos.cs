using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace FRRJIf
{
    public class DataCurPos
    {
        internal DataCurPos()
        {
        }

        public FRIF_DATA_TYPE DataType { get; }

        public int Group { get; }

        public DataTable DataTable { get; }

        public bool Valid { get; }

        public bool GetValue(ref Array Xyzwpr, ref Array Config, ref Array Joint, ref short UF, ref short UT, ref short ValidC, ref short ValidJ)
        {
            return true;
        }

        public int ObjectID { get; }

        public void Kill()
        {
        }

        public string GetTypeName()
        {
            return "";
        }

        public void AddUserMessage(string vstrMessage)
        {
        }

        public bool DebugLog { get; }

        public bool GetValueXyzwpr(ref float X, ref float Y, ref float Z, ref float W, ref float P, ref float R, ref float E1, ref float E2, ref float E3, ref short C1, ref short C2, ref short C3, ref short C4, ref short C5, ref short C6, ref short C7, ref short UF, ref short UT, ref short ValidC)
        {
            return true;
        }

        public bool GetValueJoint(ref float J1, ref float J2, ref float J3, ref float J4, ref float J5, ref float J6, ref float J7, ref float J8, ref float J9, ref short UT, ref short ValidJ)
        {
            return true;
        }
    }
}